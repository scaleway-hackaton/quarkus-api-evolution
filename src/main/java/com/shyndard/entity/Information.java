package com.shyndard.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Information {

    private String title;
    private String message;
    private String status;

    public Information() {

    }

    public Information(String title, String message, String status) {
        this.title = title;
        this.message = message;
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public Information setTitle(String title) {
        this.title = title;
        return this;
    }

    public Information setMessage(String message) {
        this.message = message;
        return this;
    }

    public Information setStatus(String status) {
        this.status = status;
        return this;
    }
}
