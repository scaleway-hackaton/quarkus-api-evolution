package com.shyndard.controller;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.shyndard.entity.Information;
import com.shyndard.service.InformationService;

import org.eclipse.microprofile.rest.client.inject.RestClient;

import io.netty.util.internal.ThreadLocalRandom;

@Path("/api/v1/scaleway-evolution")
public class ScalewayResource {

    @Inject
    @RestClient
    InformationService informationService;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Information hello() {
        return informationService.getInformation().setStatus(ThreadLocalRandom.current().nextInt(0, 100) + " points for gryffindor !");
    }
}